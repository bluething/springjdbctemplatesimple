package com.pegipegi;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pegipegi.customer.dao.CustomerDAO;
import com.pegipegi.customer.model.Customer;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
        CustomerDAO customerDAO = (CustomerDAO) context.getBean("customerDAO");
        
        Customer customer = new Customer(3, "Habib",31);
        customerDAO.insert(customer);
        System.out.println(customer);
        
        Customer customer2 = customerDAO.findByCustomerId2(2);
        System.out.println(customer2);
        
        List<Customer> customers = customerDAO.findAll();
        for (Customer customer3 : customers) {
					System.out.println(customer3);
				}
        
        List<Customer> customers2 = customerDAO.findAll();
        for (Customer customer4 : customers2) {
        	System.out.println(customer4);
				}
        
        System.out.println("Customer name with id 1: " + customerDAO.findCustomerNameById(1));
        
        context.close();
    }
}
