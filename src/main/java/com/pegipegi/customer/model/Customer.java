/**
 * 
 */
package com.pegipegi.customer.model;

import java.io.Serializable;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class Customer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3515723245188724478L;
	
	long custId;
	String name;
	long age;
	
	public Customer(){
	}
	
	public Customer(long custId, String name, int age) {
		this.custId = custId;
		this.name = name;
		this.age = age;
	}
	
	public long getCustId() {
		return custId;
	}
	public void setCustId(long custId) {
		this.custId = custId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getAge() {
		return age;
	}
	public void setAge(long age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Customer [age=" + age + ", custId=" + custId + ", name=" + name
				+ "]";
	}
	
}
